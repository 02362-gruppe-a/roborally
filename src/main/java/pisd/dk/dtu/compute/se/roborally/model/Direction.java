package pisd.dk.dtu.compute.se.roborally.model;

/**
 *
 * @author Gustav Utke Kauman, s195396@student.dtu.dk
 */
public enum Direction {

    LEFT, RIGHT;

}
